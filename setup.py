import os
from setuptools import find_packages, setup

with open(os.path.join(os.path.dirname(__file__), 'README.rst')) as readme:
    README = readme.read()

# allow setup.py to be run from any path
os.chdir(os.path.normpath(os.path.join(os.path.abspath(__file__), os.pardir)))

setup(
    name='devteam_utils',
    version='1.0.3',
    packages=find_packages(),
    include_package_data=True,
    license='BSD License',  
    description='Utilidades de desarrollo para python 3.6',
    long_description=README,
    url='',
    author='DevTeaam alias DreamTeam',
    author_email='dream.team@mail.not',
    classifiers=[
        'Environment :: Web Environment',
        'Framework :: Django',
        'Framework :: Django :: 2.0',  # replace "X.Y" as appropriate
        'Intended Audience :: Developers',
        'License :: OSI Approved :: BSD License',  # example license
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
        'Topic :: Internet :: WWW/HTTP',
        'Topic :: Internet :: WWW/HTTP :: Dynamic Content',
    ],
	install_requires=[
		'beautifulsoup4==4.6.0',
		'requests==2.13.0'
	],
)