====================================================================
Changelog
====================================================================
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

-----------
+ Added
/ Modified
- Deleted
-----------

[1.0.0] - 2018-08-01
-------------------------------------------------------

[1.0.1] - 2018-10-05
/ Emailer - enviar a multiples mails

[1.0.2] - 2018-10-08
/ Emailer - Welcome + Recovery + fixes

[1.0.3] - 2018-10-15
/ Emailer - Custom connection source (host, user, password)
