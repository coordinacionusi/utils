# -*- coding: utf-8 -*-
from django.conf import settings
import logging

logger = logging.getLogger(__name__)

class Logger(object):

    @staticmethod
    def exception(msg='' , ex=None, module='', action='', id=0, user=None, extra_dict={}):
        user_msg = ''

        try:
            msg = msg if (msg!= None and msg!='') else '¡Ha ocurrido un error!'
            user_msg = msg #mensaje para el usuario

            if settings.DEBUG and ex!=None:
                user_msg = str(ex)

            #extra
            ed= {'user': user, 'id': id}
            ed.update(extra_dict)

            #Logger
            if logger !=None:
                if(ex != None):
                    logger.exception(" ".join([module, action, str(ex)]), extra = ed)
                else:
                    logger.exception(" ".join([module, action, msg]), extra = ed)

        except Exception as e:
            pass

        return user_msg

    @staticmethod
    def info(msg):
        logger.info(msg, extra={'user': None, 'id': 0})
