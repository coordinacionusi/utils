from django.conf import settings

"""
    General context processor, agrega los siguientes atributos a
    cada context
"""
def general_context_processor(request):
    info = {
        'project_abrv': settings.PROJECT_ABRV,
        'project_title': settings.PROJECT_TITLE,
        'project_version': settings.PROJECT_VERSION,
        'header_text': settings.HEADER_TEXT,
        'footer_text': settings.FOOTER_TEXT,
        'google_maps_key':settings.GOOGLE_MAPS_KEY,
        'navbar_class':settings.NAVBAR_CLASS,

        'google_maps_key':settings.GOOGLE_MAPS_KEY, 
        'recaptcha_public_key':settings.RECAPTCHA_PUBLIC_KEY,
    }

    return info
