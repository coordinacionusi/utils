import os, uuid
from django.conf import settings

def get_formato_path(instance, filename):
    ext = filename = filename.split('.')[-1]
    filename = "%s.%s" % (uuid.uuid4(), ext)
    return os.path.join(settings.FORMATOS_ROOT, filename)


def get_pictures_path(instance, filename):
    ext = filename = filename.split('.')[-1]
    filename = "%s.%s" % (uuid.uuid4(), ext)
    return os.path.join(settings.MEDIA_ROOT, str(instance.id), filename)


def get_tramite_path(instance, filename):
    ext = filename = filename.split('.')[-1]
    filename = "%s.%s" % (uuid.uuid4(), ext)
    return os.path.join(settings.TRAMITE_ROOT, filename)

"""
Logotipo de la dependencia
"""
def get_logo_path(instance, filename):
    ext = filename = filename.split('.')[-1]
    filename = "%s.%s" % (uuid.uuid4(), ext)
    dependencia_id = str(instance.id)
    return os.path.join(settings.DEPENDENCIA_ROOT, dependencia_id, filename)


"""
Fachadas de las oficinas
"""
def get_fachada_path(instance, filename):
    ext = filename = filename.split('.')[-1]
    filename = "%s.%s" % (uuid.uuid4(), ext)
    dependencia_id = ''
    if hasattr(instance, 'dependencia'):
        dependencia_id = str(instance.dependencia.id)

    return os.path.join(settings.DEPENDENCIA_ROOT, dependencia_id, str(instance.id), filename)


"""
    Avatares de usuario
"""
def get_avatar_path(instance, filename):
    ext = filename = filename.split('.')[-1]
    filename = "%s.%s" % (uuid.uuid4(), ext)
    dependencia_id = ''
    if hasattr(instance, 'dependencia'):
        dependencia_id = str(instance.dependencia.id)

    return os.path.join(settings.AVATARES_ROOT, dependencia_id, filename)


def get_archivo_auxiliar(instance, filename):
    ext = filename = filename.split('.')[-1]
    filename = "%s.%s" % (uuid.uuid4(), ext)
    return os.path.join(settings.AUXILIAR_ROOT, str(instance.solicitud.pk), filename)