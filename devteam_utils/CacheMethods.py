
def calculate_solicitud_cache_key(view_instance, view_method,
                        request, args, kwargs):
        return '.'.join([
            's',
            kwargs.get('curp')
        ])
