import requests, json
from django.conf import settings


class CaBaGetter(object):
    @staticmethod
    def get_data_of_Vigencia():
        try:
            url = settings.CABA_HOST + settings.CAT_VIGENCIAS
            headers = {'Authorization': 'Token ' + settings.CABA_TOKEN}

            r = requests.get(url, headers=headers)

            result = json.loads(r.text)
            return result

        except Exception as e:
            return None

    @staticmethod
    def get_data_of_Tipo():
        try:
            url = settings.CABA_HOST + settings.CAT_TIPO_INFO
            headers = {'Authorization': 'Token ' + settings.CABA_TOKEN}
            r = requests.get(url, headers=headers)
            result = json.loads(r.text)
            return result

        except Exception as e:
            return None


    @staticmethod
    def get_paises():
        result = {}
        try:
            url = settings.CABA_HOST + settings.CAT_PAISES
            headers = {'Authorization': 'Token ' + settings.CABA_TOKEN}
            r = requests.get(url, headers=headers)
            result = json.loads(r.text)
            
        except Exception as e:
            result = {}

        return result


    @staticmethod
    def get_entidades():
        result = {}
        try:
            url = settings.CABA_HOST + settings.CAT_ENTIDADES
            headers = {'Authorization': 'Token '+ settings.CABA_TOKEN}

            r = requests.get(url, headers=headers)

            result = json.loads(r.text)
            
        except Exception as e:
            result = {}

        return result


    @staticmethod
    def get_municipios(cve_ent=None):
        result = {}
        try:
            url = settings.CABA_HOST + settings.CAT_MUNICIPIOS
            if cve_ent:
                url += "?cve_ent=" + cve_ent

            headers = {'Authorization': 'Token '+settings.CABA_TOKEN}
            r = requests.get(url, headers=headers)
            result = json.loads(r.text)
            
        except Exception as e:
            result = {}
        
        return result


    @staticmethod
    def get_localidades(cve_ent=None, cve_mun=None):
        result = {}
        try:
            qs_symbol="?"
            url = settings.CABA_HOST + settings.CAT_LOCALIDADES
            if cve_ent:
                url += qs_symbol + "cve_ent=" + cve_ent
                qs_symbol = "&"
            if cve_mun:
                url += qs_symbol + "cve_mun=" + cve_mun
            
            headers = {'Authorization': 'Token ' + settings.CABA_TOKEN}
            r = requests.get(url, headers=headers)
            result = json.loads(r.text)
            
        except Exception as e:
            result = {}
        
        return result


    @staticmethod
    def get_tipo_asentamiento():
        result = {}

        try:
            url = settings.CABA_HOST + settings.CAT_TIPO_ASENT
            headers = {'Authorization': 'Token '+settings.CABA_TOKEN}
            r = requests.get(url, headers=headers)
            result = json.loads(r.text)
            
        except Exception as e:
            result = {}
        
        return result

    
    @staticmethod
    def get_tipo_vialidades():
        result = {}
        try:
            url = settings.CABA_HOST + settings.CAT_TIPO_VIAL
            headers = {'Authorization': 'Token '+settings.CABA_TOKEN}
            r = requests.get(url, headers=headers)
            result = json.loads(r.text)
            
        except Exception as e:
            result = {}
        
        return result




