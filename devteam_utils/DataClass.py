import json


class DataClass:
    def __init__(self, draw, length, start,search_value,search_regex,columnas,orders,search=False):
        self.draw = draw
        self.start = start
        self.length = length
        self.columns = columnas
        self.order = orders
        self.search_value = search_value
        self.search_regex = search_regex
        self.error = ""

    def JsonReturn(self, datosTabla, pData=None):
        respuesta = {
            "draw":self.draw,
            "recordsTotal": self.recordsTotales,
            "recordsFiltered":self.recordsTotales,
            "data": datosTabla,
            "error":self.error,
            "pData": pData
        }
        return json.dumps(respuesta)


    def SimpleReturn(self, datosTabla, pData=None):
        respuesta = {
            "draw":self.draw,
            "recordsTotal": self.recordsTotales,
            "recordsFiltered":self.recordsTotales,
            "data": datosTabla,
            "error":self.error,
            "pData": pData
        }

        return respuesta