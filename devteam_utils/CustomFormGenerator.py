from bs4 import BeautifulSoup, NavigableString, Tag
import json, logging


logger = logging.getLogger(__name__)


class CustomFormGenerator(object):
    """
    Toma el html generado por el FormBuilder y regresa un html sin los formularios de control
    y con las clases y prefijos necesarios. No cambia el html original, solo abusa sexualmente de
    el y lo abandona.
    """
    @staticmethod
    def generate_rendered_form(unprocessed_html):
        prefix_name = 'custom-element-'
        cunt = 0
        souped = BeautifulSoup(unprocessed_html, 'html.parser')
        for element_tag in souped.findAll("div", {"class": "form-group"}):
            # 1 Remove data-content because we don't need that in the render
            if 'data-content' in element_tag.attrs:
                del element_tag.attrs['data-content']
            # 2 Pimp each element (Set name and style)
            for element_input in element_tag.findChildren('input'):
                if 'type' in element_input.attrs and element_input.attrs['type']=='text':
                    element_input.attrs['name'] = prefix_name + str(cunt)
                    cunt+=1
                if 'type' in element_input.attrs and element_input.attrs['type']=='radio' and not "bea" in element_tag.attrs["class"]:
                    CustomFormGenerator.style_radio(element_tag, souped, prefix_name)

                if 'type' in element_input.attrs and element_input.attrs['type']=='checkbox':
                    CustomFormGenerator.style_checkbox(element_tag, souped, prefix_name, cunt)

            for element_input in element_tag.findChildren('select'):
                element_input.attrs["class"].append("select2")
                element_input.attrs['name'] = prefix_name + str(cunt)
                cunt+=1

            for element_input in element_tag.findChildren('textarea'):
                element_input.attrs['name'] = prefix_name + str(cunt)
                cunt+=1


        p = souped.prettify()
        return '<div class="form-horizontal">' + p + '</div>' if len(p) > 0 else None


    @staticmethod
    def style_checkbox(element, souped, prefix, cunt):
        """
        This method takes the form builder natural checkbox and turns it into this:

        <div class="checkbox">
		    <label>
			    <input type="checkbox" class="styled" checked="checked">
			    Checked styled
		    </label>
	    </div>
        <div class="" ="true" ="" rel="popover" style="" title="" trigger="manual"> 
            <div class="checkbox"> 
                <label> 
                    <input class="styled" name="custom-element-2" value="checkbox_option" type="checkbox">
                    Casilla
                </label>
            </div>
        </div>

        """
        text_label = element.findChildren("label", {"class":"control-label"})
        input = element.findChildren("input")
        # extract and store valuable items
        if len(text_label) > 0:
            text_label = text_label[0].extract()
        
        if len(input) > 0:
            input = input[0].extract()
            input.attrs["name"] = prefix + str(cunt)
            input.attrs["class"][:] = []
            input.attrs["class"].append("styled")
        # dispose of the rest
        element.clear()

        # Create divs
        div_check = souped.new_tag("div", **{"class":"checkbox"})
        div_label = souped.new_tag("label")

        # Set wrapping elements
        element.append(div_check)
        div_check.append(div_label)

        # Set elements
        div_label.append(input)
        div_label.append(text_label.text)

    @staticmethod
    def style_radio(element, souped, prefix):
        """
        <label class="checkbox-inline">
			<input type="checkbox" checked="checked">
			Checked default
		</label>
        """
        input_list = []
        text_list = []

        label_container = element.findChildren("div", {"class": "valtype"})
        label_text = element.findChildren("label", {"class": "control-label"})

        if len(label_text) > 0:
            label_text = label_text[0]


        if len(label_container) > 0:
            for radio_element in label_container[0].findChildren("label"):

                for r_child in radio_element.findChildren("input"):
                    input_cb = r_child.extract()
                    input_cb.attrs["name"] = prefix + input_cb.attrs["name"]
                    input_list.append(input_cb)

                for r_child in radio_element.children:
                    if type(r_child) == NavigableString and len(r_child.strip()) > 0:
                        text_list.append(r_child.strip())

        element.clear()
        element.attrs["class"].append("bea")
        if len(input_list) > 0 and len(text_list) > 0 and len(text_list) == len(input_list):
            element.append(label_text)
            for i in range(len(input_list)):
                div_label = souped.new_tag("label", **{"class":"checkbox-inline"})
                div_label.append(input_list[i])
                div_label.append(text_list[i])
                element.append(div_label)


class CustomFormFinalRenderer(object):
    """
    Esta clase hija de la gran puta toma los valores key-value almacenados
    en la solicitud y los combina con el renderizado del custom form para
    mostrar el form final con las respuestas.
    """
    @staticmethod
    def combina_valores_con_form(dict_valores_str, rendered_form_html):
        try:
            data_dict = json.loads(dict_valores_str)
            souped = BeautifulSoup(rendered_form_html, 'html.parser')
        except Exception as e:
            logger.error(str(e))
            return None

        i_div = souped.new_tag('div', **{"class":"form_wrapper"})

        for i_dict in data_dict:
            for key in i_dict:
                value = i_dict[key]
                child = CustomFormFinalRenderer.find_exact_child(souped, key, value)
                label_text = CustomFormFinalRenderer.find_label_text(child)

                if child and label_text:
                    p_from_group = souped.new_tag('div', **{"class":"form-group"})
                    new_input = None
                    if child.name == "textarea":
                        new_input = souped.new_tag('textarea', **{"class":"form-control", "value":value, "disabled":"disabled"})
                        new_input.insert(0, value)
                    else:
                        value = "Si" if child.name == "input" and "type" in child.attrs and child.attrs["type"] == "checkbox" else value
                        new_input = souped.new_tag('input', **{"class":"form-control", "value":value, "disabled":"disabled"})

                    wrapper = souped.new_tag('div', **{"class":"col-md-6"})
                    label = souped.new_tag('label', **{"class":"col-md-4"})
                    label.append(NavigableString(label_text))
                    wrapper.append(new_input)
                    p_from_group.append(label)
                    p_from_group.append(wrapper)
                    i_div.append(p_from_group)


        return i_div.prettify()



    @staticmethod
    def find_exact_child(souped, key, value):
        children = souped.findChildren(attrs={"name":key})
        if len(children) == 1:
            return children[0]
        elif len(children) > 1:
            return next((ix for ix in children if "value" in ix.attrs and ix.attrs["value"]==value), None)

        return None


    @staticmethod
    def find_label_text(child):
        if not child:
            return None

        if "type" in child.attrs and child.attrs["type"] == "checkbox": # or child.attrs["type"] == "radio"):
            for r_child in child.parent.children:
                if type(r_child) == NavigableString and len(r_child.strip()) > 0:
                    return r_child.strip()

        else:
            parent = CustomFormFinalRenderer.find_parent_form_group(child, 3)
            if parent and parent.findChild(attrs={"class":"control-label"}):
                return parent.findChild(attrs={"class":"control-label"}).text.strip()

        return None

    @staticmethod
    def find_parent_form_group(element, levels=1):
        """
        Encuentra el padre especificado.
        """
        if levels == 0 or element == None:
            return None

        if 'class' in element.parent.attrs and 'form-group' in element.parent.attrs["class"]:
            return element.parent

        else:
            return CustomFormFinalRenderer.find_parent_form_group(element.parent, levels-1)