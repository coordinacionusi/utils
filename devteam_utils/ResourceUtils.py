# -*- coding: utf-8 -*-
from django.http import HttpResponse, HttpResponseNotFound
from django.conf import settings
import mimetypes
import os

"""
    Toma el logotipo de la dependencia a la que pertenece el usuario
"""
def getDependenciaLogo(request):
    logo_path = os.path.normpath(request.user.userprofile.dependencia.logo.name)
    
    if not os.path.exists(logo_path):
        logo_path = os.path.normpath('')
    
    image_data = open(logo_path, "rb").read()
    return HttpResponse(image_data, content_type="image/jpg")