import requests, json
from django.conf import settings


class IttenECGetter(object):
    @staticmethod
    def get_ciudadano_json_data(curp):
        try:
            url = settings.EC_HOST + settings.EC_DATOS_CIUDADANO_JSON + curp +"/"
            headers = {'Authorization': 'Token ' + settings.TOKEN_DATOS_GRALES}

            r = requests.get(url, headers=headers)

            result = json.loads(r.text)
            return result

        except Exception as e:
            return None


    @staticmethod
    def get_ciudadano_form(curp):
        try:
            url = settings.EC_HOST + settings.EC_DATOS_CIUDADANO_FORM + "?curp=" + curp
            headers = {'Authorization': 'Token ' + settings.TOKEN_DATOS_GRALES}

            r = requests.get(url, headers=headers)

            return r.text

        except Exception as e:
            return "<h1>No se obtuvieron los datos del ciudadano</h1>"


    @staticmethod
    def get_ciudadano_full_json_data(curp):
        try:
            url = settings.EC_HOST + settings.EC_DATOS_CIUDADANO_FULL_JSON + "?curp=" + curp
            headers = {'Authorization': 'Token ' + settings.TOKEN_DATOS_GRALES}

            r = requests.get(url, headers=headers)

            result = json.loads(r.text) if not "No encontrado" and not "Token inv" in r.text else None
            return result

        except Exception as e:
            return None


    @staticmethod
    def get_ciudadano_full_to_button_data_please(curp):
        button_list = []
        data_json = IttenECGetter.get_ciudadano_full_json_data(curp)
        
        if data_json:
            button_list.append('<button class="btn btn-primary btn-block btn-raised legitRipple copy-button" data-name="curp" data-info="' + curp +'"><i class="icon-vcard"></i> CURP</button><br />')
            button_list.append('<button class="btn btn-primary btn-block btn-raised legitRipple copy-button" data-name="nombre" data-info="' + data_json["nombre_completo"] +'"><i class="icon-user"></i> Nombre</button><br />')
            setzo = "Mujer" if data_json["sexo"] == "M" else "Hombre" 
            button_list.append('<button class="btn btn-primary btn-block btn-raised legitRipple copy-button" data-name="sexo" data-info="' + setzo +'"><i class="icon-man-woman"></i> Sexo</button><br />')

            if 'fecha_nacimiento' in data_json and len(data_json["fecha_nacimiento"]) > 0:
                button_list.append('<button class="btn btn-primary btn-block btn-raised legitRipple copy-button" data-name="fecha_nacimiento" data-info="' + '/'.join(list(reversed(data_json["fecha_nacimiento"].split('-')))) +'"><i class="icon-atom2"></i> Fecha nacimiento</button><br />')

            if 'rfc' in data_json and len(data_json["rfc"]) > 0:
                button_list.append('<button class="btn btn-primary btn-block btn-raised legitRipple copy-button" data-name="rfc" data-info="' + data_json["rfc"] +'"><i class="icon-credit-card"></i> RFC</button><br />')
                
            if 'direcciones' in data_json and len(data_json["direcciones"]) > 0:
                cunt = 0
                for direccion in data_json["direcciones"]:
                    button_list.append('<button class="btn btn-primary btn-block btn-raised legitRipple copy-button" data-name="dir_' + str(cunt) + '" data-info="' + ' '.join([direccion["calle"], direccion["numero"], direccion["colonia"], "CP", direccion["cp"]]) +'"><i class="icon-home2"></i> '+direccion["tipo_direccion"]+'</button><br />')
            
        return button_list


    @staticmethod
    def get_estatus_docos_ciudadano(instance):
        
        ilist = []
        for item in instance.informacionsolicitud_set.filter(tipo_info_display="1").all():
            ilist.append({"pk": item.fpk})

        try:
            url = settings.EC_HOST + settings.EC_ESTATUS_DOCOS_CIUDADANO + instance.curp +"/"
            headers = {'Authorization': 'Token ' + settings.TOKEN_DATOS_GRALES, 'Content-Type' : "application/json"}
            r = requests.post(url, json=ilist, headers=headers)
            if r.status_code == 200: 
                return json.loads(r.text)

            return []

        except Exception as e:
            return []


    @staticmethod
    def validate_documento_informacion(curp, fpk):
        try:
            url = settings.EC_HOST + settings.EC_VALIDAR_DOCOS_CIUDADANO + curp + "/" + str(fpk) + "/"
            headers = {'Authorization': 'Token ' + settings.TOKEN_DATOS_GRALES}
            r = requests.post(url, headers=headers)
            if r.status_code == 200:
                return json.loads(r.text)
            else:
               return json.loads('{"exito": false, "msg": "No se encontraron los datos"}')

        except Exception as e:
            return json.loads('{"exito": false, "msg": "'+str(e)+'"}')



    @staticmethod
    def get_imagen_perfil(curp):
        try:
            url = settings.EC_HOST + settings.EC_IMAGEN_PERFIL_CIUDADANO + curp + "/"
            headers = {'Authorization': 'Token ' + settings.TOKEN_DATOS_GRALES}
            r = requests.get(url, headers=headers)
            if r.status_code == 200:
                return r.content
            else:
               return None

        except Exception as e:
            return None
