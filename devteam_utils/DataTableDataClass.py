import re, json

class DataTableDataClass(object):
    draw = None
    length = None
    start = None
    search_value = None
    search_regex = None
    columns = None
    order = None
    error = None

    def __init__(self, request_post):
        self.get_data_from_datatables(request_post)


    def get_data_from_datatables(self, request_post):
        columnas = {}
        orders = {}
        campo = ""
        valor = None
        for key, values in request_post.lists():
            llave = str(key)
            matchObj = re.match(r'(columns|order)\[([0-9]+)\]*', llave, re.M | re.I)
            if matchObj != None and matchObj.group() != None:
                indice = int(matchObj.group(2))
                strValor = str(values)[2:len(str(values)) - 2]
                if matchObj.group(1) == "columns":
                    if indice in columnas:
                        pass
                    else:
                        columnas[indice] = {"data":"","searchable":True,"name":"","orderable":True,"search_value":"","search_regex":True}
                    if llave.endswith("[data]"):
                        campo = "data"
                        valor = strValor
                    elif llave.endswith("[name]"):
                        campo = "name"
                        valor = strValor
                    elif llave.endswith("[searchable]"):
                        campo = "searchable"
                        valor = "true" in strValor
                    elif llave.endswith("[orderable]"):
                        campo = "orderable"
                        valor = "true" in strValor
                    elif llave.endswith("[search][value]"):
                        campo = "search_value"
                        valor = strValor
                    elif llave.endswith("[search][regex]"):
                        campo = "search_regex"
                        valor = "true" in strValor
                    columnas[indice][campo] = valor
                elif matchObj.group(1) == "order":
                    if indice in orders:
                        pass
                    else:
                        orders[indice] = {"column":-1,"dir":""}
                    if llave.endswith("[column]"):
                        orders[indice]["column"] = int(strValor)
                    elif llave.endswith("[dir]"):
                        orders[indice]["dir"] = strValor

        self.draw = int(request_post.get('draw'))
        self.length = int(request_post.get('length'))
        self.start = int(request_post.get('start'))
        self.search_value = request_post.get('search[value]').encode('utf-8')
        self.search_regex = "true" in str(request_post.get('search[regex]'))
        self.columns = columnas
        self.order = orders


    def get_orderable_column(self):
        orders = []
        for order in self.order:
            if len(self.columns[self.order[order]['column']]['name']) > 0:
                orders.append(('-' if self.order[order]['dir'] == 'desc' else '') + \
                self.columns[self.order[order]['column']]['name'])

        return orders


    def json_return(self, datosTabla, extra_data=None):
        respuesta = {
            "draw":self.draw,
            "recordsTotal": self.recordsTotales,
            "recordsFiltered":self.recordsTotales,
            "data": datosTabla,
            "error":self.error,
            "extra": extra_data
        }
        return json.dumps(respuesta)


    def simple_return(self, datosTabla, extra_data=None):
        respuesta = {
            "draw":self.draw,
            "recordsTotal": self.recordsTotales,
            "recordsFiltered":self.recordsTotales,
            "data": datosTabla,
            "error":self.error,
            "extra": extra_data
        }

        return respuesta