# -*- coding: utf-8 -*-

from django.conf import settings
from django.http import HttpResponse, Http404
from django.urls import resolve
from ipware.ip import get_ip, get_real_ip

class AdminRestrictMiddleware(object):
    """
    Este middleware se encarga de restringir el acceso al admin site
    si la petición no proviene de las ips definidas en settings.ALLOWED_ADMIN_IP.
    Regresa un 404 como si /admin no existiera.
    Esta clase es un trabajo derivado de sdonk/django-admin-ip-restrictor con la
    inspiración de la siempre alegre y hermosa Tomoko. 
    -NO BORRAR ESTE COMENTARIO-
    """
    def __init__(self, get_response):
        self.get_response = get_response


    def __call__(self, request):
        response = self.process_request(request)

        if not response and self.get_response:
            response = self.get_response(request)

        return response


    def process_request(self, request):
        if resolve(request.path).app_name == 'admin':
            ip = get_real_ip(request) or get_ip(request)
            if not ip in settings.ALLOWED_ADMIN_IP:
                raise Http404()

        return None