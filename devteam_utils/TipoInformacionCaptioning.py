# -*- coding: utf-8 -*-
import datetime, json, requests
from django.conf import settings

class TipoInformacionCaptioning(object):
    json_data = None
    data_loaded = False
    def __init__(self):
        self.get_tipo_info_from_caba()

    def get_tipo_info_from_caba(self):
        try:
            url = settings.CABA_HOST + settings.CAT_TIPO_INFO
            headers = {'Authorization': 'Token ' + settings.CABA_TOKEN}
            r = requests.get(url, headers=headers)
            self.json_data = json.loads(r.text)
            self.data_loaded = True
        except Exception as e:
            self.data_loaded = False


    def get_from_json(self, clave):
        for item in self.json_data:
            if item["clave"] == clave:
                return item["nombre"] if clave[-2:] != '00' else item["categoria_nombre"] + " (" + item["nombre"] + ")"

        return "No identificado"

