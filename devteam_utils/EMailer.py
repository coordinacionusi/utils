# -*- coding: utf-8 -*-
from django.conf import settings
from django.contrib.auth.tokens import default_token_generator
from django.contrib.sites.shortcuts import get_current_site
from django.core.mail import send_mail, EmailMultiAlternatives
from django.http import HttpRequest
from django.template.loader import get_template
from django.template import Context
from django.utils.http import urlsafe_base64_encode
from django.utils.encoding import force_bytes
from django.urls import reverse
from django.utils.encoding import python_2_unicode_compatible

@python_2_unicode_compatible
class EMailer():

    @classmethod
    def send_welcome_mail(self, request, usr, subject='', protocol=None):

        if not subject:
            subject = '¡Te damos la Bienvenida!'

        from_email = settings.DEFAULT_FROM_EMAIL

        html_template = get_template("registration/welcome_email.html")
        text_template = get_template("registration/welcome_message_subject.html")
        
        protocol = protocol if protocol != None else "http"
        current_site = get_current_site(request)

        c = {
            'email': usr.email,
            'domain': current_site.domain,
            'site_name': current_site.name,
            'uid': str(urlsafe_base64_encode(force_bytes(usr.pk))).replace("b","").replace("'",""),
            'user': usr,
			'full_name': usr.userprofile.get_full_name() if usr.userprofile else '',
            'token': default_token_generator.make_token(usr),
            'protocol': protocol,
            }

        html_content = html_template.render(c)
        text_content = text_template.render(c)

        msg = EmailMultiAlternatives(subject, text_content, from_email, [usr.email])
        msg.attach_alternative(html_content, "text/html")
        msg.send()


    @classmethod
    def send_recovery_mail(self, request, usr, subject='', protocol=None):
        
        if not subject:
            subject = 'Recuperación de Contraseña'
            
        from_email = settings.DEFAULT_FROM_EMAIL

        html_template = get_template("registration/password_reset_email.html")
        text_template = get_template("registration/password_reset_subject.html")
        
        protocol = protocol if protocol != None else "http"
        current_site = get_current_site(request)

        c = {
            'email': usr.email,
            'domain': current_site.domain,
            'site_name': current_site.name,
            'uid': str(urlsafe_base64_encode(force_bytes(usr.pk))).replace("b","").replace("'",""),
            'user': usr,
			'full_name': usr.userprofile.get_full_name() if usr.userprofile else '',
            'token': default_token_generator.make_token(usr),
            'protocol': protocol,
            }

        html_content = html_template.render(c)
        text_content = text_template.render(c)

        msg = EmailMultiAlternatives(subject, text_content, from_email, [usr.email])
        msg.attach_alternative(html_content, "text/html")
        msg.send()


    @classmethod
    def send_mail(self, request, mail, subject, context, html_template, text_template):

        subject = subject
        from_email = settings.DEFAULT_FROM_EMAIL

        html_template = get_template(html_template)
        text_template = get_template(text_template)
        
        html_content = html_template.render(context)
        text_content = text_template.render(context)

        msg = EmailMultiAlternatives(subject, text_content, from_email, [mail])
        msg.attach_alternative(html_content, "text/html")
        msg.send()
	


    @classmethod
    def send_mails(self, request, mails, subject, context, html_template, text_template):

        subject = subject
        from_email = settings.DEFAULT_FROM_EMAIL

        html_template = get_template(html_template)
        text_template = get_template(text_template)
        
        html_content = html_template.render(context)
        text_content = text_template.render(context)

        for em in mails:
            msg = EmailMultiAlternatives(subject, text_content, from_email, [em])
            msg.attach_alternative(html_content, "text/html")
            msg.send()

    @classmethod
    def send_mail_from(self, request, mail, subject, context, html_template, text_template, connection, from_email):

        subject = subject
        from_email = from_email

        html_template = get_template(html_template)
        text_template = get_template(text_template)
        
        html_content = html_template.render(context)
        text_content = text_template.render(context)

        messages = []
        msg = EmailMultiAlternatives(subject, text_content, from_email, [mail])
        msg.attach_alternative(html_content, "text/html")
        messages.append(msg)
        connection.send_messages(messages)


    @classmethod
    def send_mails_from(self, request, mails, subject, context, html_template, text_template, connection, from_email):

        subject = subject
        from_email = from_email

        html_template = get_template(html_template)
        text_template = get_template(text_template)
        
        html_content = html_template.render(context)
        text_content = text_template.render(context)

        for em in mails:
            messages = []
            msg = EmailMultiAlternatives(subject, text_content, from_email, [em])
            msg.attach_alternative(html_content, "text/html")
            messages.append(msg)
            connection.send_messages(messages)

    
		
		 