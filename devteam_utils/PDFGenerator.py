# -*- coding: utf-8 -*-
import pdfkit, re
from django.conf import settings
from django.shortcuts import reverse
from django.template.loader import get_template
import logging, datetime, qrcode, os
import base64


logger = logging.getLogger(__name__)


class PDFGenerator(object):
    """
    Clase para generar pdfs usando el rendering engine de django así como la generación de imágenes QR.
    """
    @staticmethod
    def generate_document(context, template_name):
        """
        Genera el documento pdf usando el template especificado en template_name.
        Los datos a usar por el template deben venir en context.
        """
        try:
            template = get_template(template_name)
            html = template.render(context)
            options = { 
                'page-size': 'Letter',
                'encoding': "UTF-8",
                'no-outline': None,
            }
            the_pdf = pdfkit.from_string(html, False, css=None, options=options)
            return the_pdf
        except Exception as e:
            logger.info("Error al crear el documento " + str(e) , extra={'user':None, 'id':0})

        return None


    @staticmethod
    def get_or_create_qr(unique_identifier, qr_contents):
        """
        Genera un archivo png con los datos enviados en qr_contents, nombrado como unique_identifier.png 
        en la ruta MEDIA_ROOT/qrcodes/.
        Regresa el path de la imagen o None si no se pudo generar.
        """
        path = os.path.normpath(os.path.join(settings.MEDIA_ROOT,'qrcodes', unique_identifier + ".png"))
        if os.path.isfile(path):
            return path
        else:
            validador_path = qr_contents
            new_code = qrcode.QRCode(version=3)
            new_code.add_data(validador_path)
            new_code.make(fit=True)
            img = new_code.make_image()
            try:
                img.save(path)
                return path
            except Exception as e:
                logger.error("Error al generar qr " + str(e) , extra={'user':None, 'id':0})
        
        return None
