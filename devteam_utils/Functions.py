import datetime

def get_hours():
    base = datetime.datetime.strptime('2001-01-25 06:00',"%Y-%m-%d %H:%M")
    l = [(h.strftime("%H")+":00", h.strftime("%H")+":00") for h in [base + datetime.timedelta(hours=x) for x in range(0, 17)]]
    return l
