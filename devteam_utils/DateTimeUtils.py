# -*- coding: utf-8 -*-
from django.utils import timezone
from datetime import datetime, date, time, timedelta
import math
import pytz


class DateTimeUtils(object):
    @staticmethod
    def ConvertAndMakeTimeAware(fecha):
        if fecha == None or fecha == '':
            return None
        if not isinstance(fecha, datetime):
            if isinstance(fecha, date):
                return timezone.make_aware(datetime.combine(fecha, datetime.min.time()), timezone.get_default_timezone())

            return timezone.make_aware(datetime.strptime(fecha, "%Y-%m-%d"), timezone.get_default_timezone())
        else:
            return timezone.make_aware(fecha, timezone.get_default_timezone())
            
    @staticmethod
    def ConvertAndMakeTimeAwareWithTime(fecha):
        if fecha == None or fecha == '':
            return None
        if not isinstance(fecha, datetime):
            if isinstance(fecha, date):
                return timezone.make_aware(datetime.combine(fecha, datetime.min.time()), timezone.get_default_timezone())

            return timezone.make_aware(datetime.strptime(fecha, "%Y-%m-%d %H:%M"), timezone.get_default_timezone())
        else:
            return timezone.make_aware(fecha, timezone.get_default_timezone())

    @staticmethod
    def PickOldestDate(dateList):
        return min(dateList)

    @staticmethod
    def BeautifyDate(dateObj):
        if dateObj is None:
            return "No Disponible"

        seconds = (timezone.now() - dateObj).total_seconds()
        days = seconds/86400
        if days < 1:
            hours = seconds/3600
            if hours < 1:
                return "%d minutos" % math.ceil(seconds / 60)
            return "%d horas" % math.ceil(hours)
        return "%d días" %  math.ceil(days)

    @staticmethod
    def get_today_now():
        return timezone.make_aware(datetime.now(), timezone.get_default_timezone())

    @staticmethod
    def str_to_date(date_str):
        return datetime.strptime(date_str,"%Y-%m-%d").date()

    @staticmethod
    def str_to_time(time_str):
        return datetime.strptime(time_str, '%H:%M').time()

    @staticmethod
    def int_to_time(hours, minutes, seconds):
        return time(hours, minutes, seconds)
    
    @staticmethod
    def get_timedelta(days, months=0, years=0, hours=0, minutes=0, seconds=0):
        return timedelta(days=days, seconds=seconds, hours=hours, minutes=minutes)


    @staticmethod
    def combine_date_time(date, time):
        return datetime.combine(date, time)

    @staticmethod
    def date_as_localtimezone(fecha):
        return timezone.localtime(fecha)

    @staticmethod
    def get_aware_now():
        return datetime.now(pytz.timezone("America/Mexico_City"))
